# Introduction 
This is a sample of the model Costs and Pricing - Cash Management
It was modeled and build with R.

# Getting Started
Poc:
1.	Configure local git (make shure that you have your http.proxy variable configured)
2.	Clone repository
3.	install the following librarys for R ['tidyverse', 'neuralnet'] (Requirements)
4.	load the RDS datasets and run the R script files inside the 'script' folder.

# Build and Test
The build is already done, it lays in the studyCase folder. 
For safety and compliance reasons, it cannot be public.
For detailed information, send a e-mail for sdcampos@santander.com.br