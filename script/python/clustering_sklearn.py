__author__ = 't686138'

from sklearn import metrics
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale, LabelEncoder, OneHotEncoder, LabelBinarizer
from sklearn.model_selection import train_test_split
# from sklearn.metrics.classification import precision_recall_fscore_support

import datetime
import itertools
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.mlab as mlab
import seaborn as sns
import pandas as pd

from pandas.tools.plotting import scatter_matrix

from copy import deepcopy
from pprint import pprint

debugMode = True

if __name__ == "__main__":

    attributes = ["Custo/Liq", "Auto-Atendimento", "Caixa",
                  "Call Center", "Correspondente", "Demais Canais", "Internet",
                  "PagFor", "classe"]

    data_file = "C:\SVN_trunk\Santander%20AI\cashCobros\dataset\sample.csv"
    # data_file = "../../dataset/sample.csv"
    df_cobros = pd.read_csv(data_file, sep=",")

    if DebugMode:
        print("Some basic Info about the dataframe:")
        pprint(df_cobros.info())

        print("\nSome stats:")
        pprint(df_cobros.describe())

        print "\nFirst 5 elements:"
        pprint(df_cobros.head())


    # original_headers = list(df_cobros.columns.values)

    class_cat = df_cobros["classe"]
    
    # encoder = LabelEncoder()
    # class_cat_encoded = encoder.fit_transform(class_cat)
    # print(encoder.classes_)

    # encoder = OneHotEncoder()
    # class_cat_1Hot = encoder.fit_transform(class_cat_encoded.reshape(-1,1))
    # print(class_cat_1Hot.toarray())

    encoder = LabelBinarizer()
    class_cat_1Hot = encoder.fit_transform(class_cat)
    # print(class_cat_1Hot)
