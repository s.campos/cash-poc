require(tidyverse)
require(ggplot2)

# Base data frame @ ./dataset/gcbCob.RDS

gcb_df <- read_rds("./dataset/gcbCob.RDS")

gcb_df %>% 
  drop_na(Xbar_Liquidacoes) %>% 
  filter(!CNPJ_CPF %in% c('033066408000115','007707650000110')) %>% 
  filter(Xbar_Liquidacoes >= 50) %>% 
  filter(Xbar_Tarifa <= 10) %>% 
  mutate_if(is.numeric, funs(ifelse(is.na(.), 0, .))) -> gcb_preCluster

gcb_preCluster$log_boletos <- log(gcb_preCluster$Xbar_Liquidacoes) 

kmeans(gcb_preCluster[,c(7,15)], 3)

scaled_data <- as.matrix(scale(gcb_preCluster[,c(7,15)]))
scaled_data <- scale(gcb_toKmeans[,8:14])
k.max <- 15
wss <- sapply(1:k.max,
              function(k){
                kmeans(scaled_data, k, nstart = 50, iter.max = 15)$tot.withinss
              })
plot(1:k.max, wss,
     type="b", pch = 19, frame = FALSE, 
     xlab="Number of clusters K",
     ylab="Total within-clusters sum of squares")

gcb_preCluster$cluster <- kmeans(gcb_preCluster[,c(7,15)], 4)$cluster

# Load Cannonic Dataset

canon <- read_rds("./dataset/canon.rds")

ggplot(canon, aes(x = log_boletos, y = `Custo/Liq`)) +
  geom_point(aes(color=canon$classe))

canon_cluster <- kmeans(canon[,c(2,4)], 4)
canon$cluster <- canon_cluster$cluster

ggplot(canon, aes(x = log_boletos, y = `Custo/Liq`)) +
  geom_point(aes(color=as.factor(canon$cluster))) + 
  geom_point(aes(x = 4.877715219, y = 0.9607142857), color='blue', size=5) + 
  geom_point(aes(x = 7.361980455, y = 1.2405882353), color='red', size=5) +
  geom_point(aes(x = 4.989407627, y = 2.6033333333), color='green', size=5) + 
  geom_point(aes(x = 10.912577103, y = 1.6611764706), color='gray', size=5)

ggplot(canon, aes(x = log_boletos, y = `Custo/Liq`)) +
  geom_point(aes(color=as.factor(canon$cluster))) +
  geom_point(aes(x = canon_cluster$centers[5] , y = canon_cluster$centers[1]), size=5, shape = 10) + 
  geom_point(aes(x = canon_cluster$centers[6], y = canon_cluster$centers[2]), size=5, shape = 10) +
  geom_point(aes(x = canon_cluster$centers[7], y = canon_cluster$centers[3]), size=5, shape = 10) + 
  geom_point(aes(x = canon_cluster$centers[8], y = canon_cluster$centers[4]), size=5, shape = 10)
